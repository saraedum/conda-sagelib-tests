#!/bin/bash

#source /opt/docker/bin/entrypoint_source

set -ex
git clone https://github.com/sagemath/sage.git sagemath
cd sagemath
git checkout 8.8
git clone https://github.com/conda-forge/sagelib-feedstock.git
for p in `grep -v '#' sagelib-feedstock/recipe/patches/series`;do
    patch -p1 < ./sagelib-feedstock/recipe/patches/"$p"
done

conda config --add channels conda-forge
# We have to pin to NetworkX 2.1 for Sage 8.3, otherwise we pull an old sagelib build
conda create -p `pwd`/local "sage=8.8" python=2 gdb

