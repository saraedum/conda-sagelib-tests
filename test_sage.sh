#!/bin/bash

cd sagemath

#source /opt/docker/bin/entrypoint_source
source activate `pwd`/local
export SAGE_LOCAL=`pwd`/local
export SAGE_ROOT=`pwd`
export CFLAGS="$CFLAGS -I/usr/include"
export CXXFLAGS="$CXXFLAGS -I/usr/include"
export CPPFLAGS="$CPPFLAGS -I/usr/include"
ls $SAGE_ROOT
ls $SAGE_LOCAL
rm "$SAGE_LOCAL/lib/sage-current-location.txt"

(sage -tp --initial --all --optional=memlimit,sage | tee doctest.txt) || true

conda list '^sagelib$'
conda list '^sage$'
cat doctest.txt | grep '^sage -t ' | grep '#' > doctest.summary.txt || true
conda install -y bc
echo `cat doctest.summary.txt | awk '{ print $5 }' | sort | grep -E '[0-9]+' | paste -sd+ | bc` failing individual doctests
cat doctest.summary.txt | awk '{ print $5 }' | sort | grep -vE '[0-9]+' | sort | uniq -c || true
